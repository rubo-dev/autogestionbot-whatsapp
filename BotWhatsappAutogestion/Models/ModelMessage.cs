﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotWhatsappAutogestion.Models
{
    public class ModelMessage
    {
        [JsonProperty("alcance")] public string Alcance;

        [JsonProperty("anioAcademico")] public string AnioAcademico;

        [JsonProperty("antiguedad")] public string Antiguedad;

        [JsonProperty("archivoFisico")] public string ArchivoFisico;

        [JsonProperty("bloqueaEmail")] public string BloqueaEmail;

        [JsonProperty("comision")] public string Comision;

        [JsonProperty("data")] public string Data;

        [JsonProperty("de")] public int De;

        [JsonProperty("emailDestino")] public string EmailDestino;

        [JsonProperty("emailOrigen")] public string EmailOrigen;

        [JsonProperty("especialidad")] public string Especialidad;

        [JsonProperty("fechaPublicacion")] public long FechaPublicacion;

        [JsonProperty("fechaPublicacionCompleta")]
        public long FechaPublicacionCompleta;

        [JsonProperty("foto")] public string Foto;

        [JsonProperty("id")] public int Id;

        [JsonProperty("materia")] public string Materia;

        [JsonProperty("mensaje")] public string Mensaje;

        [JsonProperty("mensajeContactoAutorizacionEstadoDescripcion")]
        public string MensajeContactoAutorizacionEstadoDescripcion;

        [JsonProperty("mensajeContactoAutorizacionEstadoId")]
        public string MensajeContactoAutorizacionEstadoId;

        [JsonProperty("mensajeTipo")] public string MensajeTipo;

        [JsonProperty("nombre")] public string Nombre;

        [JsonProperty("nombreUsuarioPublica")] public string NombreUsuarioPublica;

        [JsonProperty("nota")] public string Nota;

        [JsonProperty("para")] public int Para;

        [JsonProperty("pideAutorizacion")] public string PideAutorizacion;

        [JsonProperty("plan")] public string Plan;

        [JsonProperty("respuestaAmensajeId")] public string RespuestaAmensajeId;

        [JsonProperty("tieneAdjunto")] public int TieneAdjunto;


        public override string ToString()
        {
            var fechaPublicacionMensaje = DateTimeOffset.FromUnixTimeMilliseconds(FechaPublicacionCompleta).DateTime.AddHours(-3);

            var output =
                $"📩 _[{fechaPublicacionMensaje.ToString("HH:mm dd/MM/yyyy")}]_" +
                $"\n*{(Alcance.Equals("mí") ? "Autogestion" : Alcance)}, {Nombre ??= "Sistema"} dijo:*" +
                $" \n\n{Mensaje}";

            return output;
        }

    }
}
