﻿using System;
using BotWhatsappAutogestion.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace BotWhatsappAutogestion.DataLayer
{
    public partial class DBManager : DbContext
    {
        public DBManager()
        {
        }

        public DBManager(DbContextOptions<DBManager> options)
            : base(options)
        {
        }

        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(BotSettings.DBConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "en_US.UTF-8");

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.WhatsappNumber)
                    .HasName("USERS_pkey");

                entity.ToTable("USERS", "AUTOGESTION_BOT_DATA_WHATSAPP");

                entity.Property(e => e.WhatsappNumber).HasColumnName("whatsappNumber");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.AutomaticoActive).HasColumnName("automaticoActive");

                entity.Property(e => e.AutomaticoPermitido).HasColumnName("automaticoPermitido");

                entity.Property(e => e.Especialidad)
                    .IsRequired()
                    .HasColumnName("especialidad");

                entity.Property(e => e.FechaUltimaRequest)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("fechaUltimaRequest");

                entity.Property(e => e.FechaUltimoMensaje)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("fechaUltimoMensaje");

                entity.Property(e => e.Legajo)
                    .IsRequired()
                    .HasColumnName("legajo");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password");

                entity.Property(e => e.TiempoActualizacion)
                    .HasColumnName("tiempoActualizacion")
                    .HasDefaultValueSql("60");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
