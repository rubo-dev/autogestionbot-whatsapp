﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BotWhatsappAutogestion.DataLayer
{
    public partial class User
    {
        public string Especialidad { get; set; }
        public DateTime FechaUltimaRequest { get; set; }
        public DateTime FechaUltimoMensaje { get; set; }
        public string Legajo { get; set; }
        public string Password { get; set; }
        public string WhatsappNumber { get; set; }
        public bool Activo { get; set; }
        public int? TiempoActualizacion { get; set; }
        public bool AutomaticoActive { get; set; }
        public bool AutomaticoPermitido { get; set; }
    }
}
