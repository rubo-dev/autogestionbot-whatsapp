﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotWhatsappAutogestion.Exceptions
{
    public class AutogestionException : Exception
    {
        public AutogestionException(string message) : base(message)
        {
        }

        public AutogestionException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
