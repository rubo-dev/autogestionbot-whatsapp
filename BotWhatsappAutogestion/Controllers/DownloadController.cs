﻿using BotWhatsappAutogestion.Exceptions;
using BotWhatsappAutogestion.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace BotWhatsappAutogestion.Controllers
{
    [Route("download")]
    public class DownloadController : Controller
    {
        [HttpGet]
        public IActionResult Download(string id, string whatsappNumber, string fileName)
        {
            DataLayer.User userWithCredentials = DBHelper.GetCredentialsForUser(whatsappNumber);
            Stream stream = null;

            try
            {
                stream = AutogestionService.DescargarDocumento(userWithCredentials.Legajo, userWithCredentials.Password, userWithCredentials.Especialidad, id);
            }
            catch (InvalidCredentialException e)
            {
                Console.WriteLine($"[CREDENTIALS] {e.GetType().Name}: {e.Message}\n{e.StackTrace}");
                WhatsAppService.SendMessage(whatsappNumber, $"{e.Message}\nEnviá login con la contraseña correcta nuevamente.");
            }
            catch (AutogestionException e)
            {
                Console.WriteLine($"[CREDENTIALS] {e.GetType().Name}: {e.Message}\n{e.StackTrace}");
                WhatsAppService.SendMessage(whatsappNumber,
                    $"‼Ocurrió un problema al descargar el documento {fileName}. \n\n❌Puede ser porque autogestión no esté disponible en este momento. \n\n" +
                    "Recordá que si cambiaste la contraseña el bot no tiene forma de averiguarlo. Si es así, logueate de nuevo con _*login*_ !\n" +
                    "Gracias por usar el bot, y lamentamos el error 🤗");
            }
            catch (Exception e)
            {
                Console.WriteLine($"[CREDENTIALS] {e.GetType().Name}: {e.Message}\n{e.StackTrace}");
                WhatsAppService.SendMessage(whatsappNumber, "Ocurrió un error, contacte a los devs: " + e.Message);
            }

            if (stream == null)
                return NotFound();

            //Obtener MIME type
            var provider = new FileExtensionContentTypeProvider();

            if (!provider.TryGetContentType(fileName, out string contentType))
            {
                contentType = "application/octet-stream";
            }

            return new FileStreamResult(stream, new MediaTypeHeaderValue(contentType))
            {
                FileDownloadName = fileName
            };
        }
    }
}
