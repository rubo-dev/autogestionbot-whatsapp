﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio.AspNet.Core;
using Twilio.TwiML;
using Twilio.AspNet.Common;
using Twilio.Rest.Api.V2010.Account;
using BotWhatsappAutogestion.Services;
using BotWhatsappAutogestion.Logic;

namespace BotWhatsappAutogestion.Controllers
{
    [Route("whatsapp")]
    public class WhatsAppController : TwilioController
    {
        [HttpPost]
        public TwiMLResult Index(SmsRequest request)
        {
            MessagingResponse response = new MessagingResponse();

            string baseURL = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";

            response.Message(MessageProcesser.ProcessNewMessageFromBot(request, baseURL));

            return TwiML(response);
        }
    }
}
