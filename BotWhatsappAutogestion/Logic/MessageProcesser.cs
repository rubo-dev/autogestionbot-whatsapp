﻿using BotWhatsappAutogestion.Exceptions;
using BotWhatsappAutogestion.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using Twilio.AspNet.Common;

namespace BotWhatsappAutogestion.Logic
{
    public class MessageProcesser
    {
        private static Dictionary<string, DateTime> lastRequestFromUser = new Dictionary<string, DateTime>();

        public static string ProcessNewMessageFromBot(SmsRequest request, string baseUrl)
        {
            string senderWhatsappNumber = request.From;

            if (request == null)
            {
                Console.WriteLine($"[{senderWhatsappNumber}]sent an empty message");
                return "Hubo un problema al procesar tu mensaje.";
            }

            var mensaje = request.Body.ToLower();

            if (string.IsNullOrEmpty(request.Body) || (!mensaje.StartsWith("login") && !mensaje.Equals("ayuda") && !mensaje.Equals("automatico") && !mensaje.Equals("actualizar") && !mensaje.StartsWith("descargar")))
            {
                Console.WriteLine($"[{senderWhatsappNumber}]says: {mensaje}");
                return "Hubo un problema al procesar tu mensaje.\n\n" +
                    "Para iniciar sesión enviá _*login*_ con tu legajo, contraseña y especialidad separados por espacios \n\npor ejemplo:\nlogin 67890 micontraseña sistemas\n\n" +
                    "Si ya iniciaste sesión enviá _*actualizar*_ para buscar si hay nuevos mensajes para vos en autogestión\n\n" +
                     "También podés hacer uso del comando _*automatico*_ para recibir de manera automática los mensajes del autogestión\n\n" +
                     "Si necesitás ver este mensaje de ayuda otra vez mandá _*ayuda*_\n\n" +
                     "🔐 Quedate tranquilo, tu contraseña está protegida punto a punto por cifrado AES-256, ni siquiera los desarrolladores podemos verla!\n" +
                     "\n❔Cualquier duda que tengas, podés hablar con los devs para obtener mas información, somos alumnos de la facu igual que vos\n\n" +
                     "También podés probar una versión de este bot con más funciones en Telegram\nhttps://t.me/autogestion_notificador_bot\n\n" +
                     "Contacto (Telegram):\nhttps://t.me/LucioB16\nhttps://t.me/Cyro_R";
            }

            if (!lastRequestFromUser.ContainsKey(senderWhatsappNumber) ||
                lastRequestFromUser[senderWhatsappNumber] == null)
            {
                lastRequestFromUser.Add(senderWhatsappNumber, DateTime.MinValue);
            }

            var diffEntrePeticiones = DateTime.UtcNow.Subtract(lastRequestFromUser[senderWhatsappNumber]);

            if (diffEntrePeticiones <= TimeSpan.FromSeconds(3))
            {
                lastRequestFromUser[senderWhatsappNumber] = DateTime.UtcNow;

                return "Estás enviando peticiones muy rápido, el server ta chikito 😭\n\nEsperá en unos 3s entre cada solicitud";
            }

            lastRequestFromUser[senderWhatsappNumber] = DateTime.UtcNow;

            DBHelper.UpdateLastRequestDate(senderWhatsappNumber);

            if (!mensaje.StartsWith("login")) Console.WriteLine($"[{senderWhatsappNumber}]says: {mensaje}");
            else Console.WriteLine($"[{senderWhatsappNumber}] - Login Requested (password protect)");

            if (mensaje.Equals("login") || mensaje.Equals("ayuda"))
            {
                return "Para iniciar sesión enviá _*login*_ con tu legajo, contraseña y especialidad separados por espacios \n\npor ejemplo:\nlogin 67890 micontraseña sistemas\n\n" +
                     "Si ya iniciaste sesión enviá _*actualizar*_ para buscar si hay nuevos mensajes para vos en autogestión\n\n" +
                     "También podés hacer uso del comando _*automatico*_ para recibir de manera automática los mensajes del autogestión\n\n" +
                     "Si necesitás ver este mensaje de ayuda otra vez mandá _*ayuda*_\n\n" +
                     "🔐 Quedate tranquilo, tu contraseña está protegida punto a punto por cifrado AES-256, ni siquiera los desarrolladores podemos verla!\n" +
                     "\n❔Cualquier duda que tengas, podés hablar con los devs para obtener mas información, somos alumnos de la facu igual que vos\n\n" +
                     "También podés probar una versión de este bot con más funciones en Telegram\nhttps://t.me/autogestion_notificador_bot\n\n" +
                     "Contacto (Telegram):\nhttps://t.me/LucioB16\nhttps://t.me/Cyro_R";
            }

            if (mensaje.StartsWith("login")) new Task(() => AsyncFunctions.GenerarMensajeLogin(request)).Start();


            if (mensaje.Equals("automatico"))
            {
                WhatsAppService.SendMessage(senderWhatsappNumber, "Procesando solicitud...");
                var activeUser = DBHelper.ActiveUser(senderWhatsappNumber);
                if (!activeUser)
                {
                   return "Su usuario se encuentra inactivo/baneado por el momento no puede usar esta funcion";
                }

                var authorizedDaemon = DBHelper.IsAuthorizedAutomaticRetrieve(senderWhatsappNumber);
                if (!authorizedDaemon)
                {
                    return "⚠Tu usuario no tiene permisos aun para usar la funcion de recuperacion automatica de mensajes.\n\n" +
                        "Por el momento solo algunos usuarios tienen este privilegio, debido a que son los que ayudan a desarrollar y mantener el servidor de hosting de este bot.\n" +
                        "\nEstá planeado que en el futuro esta funcion este disponible para todos\n" +
                        "Contactá a los devs si querés tener acceso o si sos amigo 🤣 asi te damos permiso.\n" +
                        "Podés ver nuestros contactos enviando la palabra _*ayuda*_";
                }

                var daemon = new AGDaemon(senderWhatsappNumber);
                daemon.DaemonActive = !DBHelper.AutomaticIsActive(senderWhatsappNumber); //cambia el estado
                DBHelper.SetAutomaticState(senderWhatsappNumber, daemon.DaemonActive);
                if (daemon.DaemonActive) new Task(() => daemon.Run()).Start();
                else
                    return "‼ Se desactivó la recuperacion automatica de mensajes ‼";                
            }

            if (mensaje.Equals("actualizar"))
            {
                var userDB = DBHelper.GetCredentialsForUser(senderWhatsappNumber);
                try
                {
                    var mensajes = new AutogestionService().ObtenerMensajes(userDB.Legajo, userDB.Password, userDB.Especialidad);
                    AsyncFunctions.EnviarMensajesAutogestion(senderWhatsappNumber, mensajes);
                }
                catch (InvalidCredentialException e)
                {
                    Console.WriteLine($"[CREDENTIALS] {e.GetType().Name}: {e.Message}\n{e.StackTrace}");
                    WhatsAppService.SendMessage(senderWhatsappNumber, $"{e.Message}\nEnviá _*login*_ con usuario, contraseña y especialidad correcta nuevamente.");
                }
                catch (AutogestionException e)
                {
                    Console.WriteLine($"[CREDENTIALS] {e.GetType().Name}: {e.Message}\n{e.StackTrace}");
                    WhatsAppService.SendMessage(senderWhatsappNumber,
                        "‼Ocurrió un problema mientras procesabamos tu solicitud. \n\n❌Puede ser porque autogestión no esté disponible en este momento. \n\n" +
                        "Recordá que si cambiaste la contraseña el bot no tiene forma de averiguarlo. Si es así, logueate de nuevo con _*login*_ !\n" +
                        "Gracias por usar el bot, y lamentamos el error 🤗");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"[CREDENTIALS] {e.GetType().Name}: {e.Message}\n{e.StackTrace}");
                    WhatsAppService.SendMessage(senderWhatsappNumber, "Ocurrió un error, contacte a los devs: " + e.Message);
                }
                return string.Empty;
            }

            if (mensaje.Equals("descargar"))
            {
                return "Recordá enviar el mensaje de descarga tal cual lo genera el link";
            }
            else
            {
                if (mensaje.StartsWith("descargar"))
                {
                    AsyncFunctions.EnviarArchivoRequest(request, baseUrl);
                    return "⬇⬆📄 Estamos descargando y enviandote el documento... esta tarea puede tardar un poco";
                }
            }

            Console.WriteLine("[SYSTEM] End of Request " + senderWhatsappNumber);
            return string.Empty;
        }
    }
}
