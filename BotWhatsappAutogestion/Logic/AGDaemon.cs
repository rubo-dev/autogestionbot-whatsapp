﻿using BotWhatsappAutogestion.Exceptions;
using BotWhatsappAutogestion.Models;
using BotWhatsappAutogestion.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;

namespace BotWhatsappAutogestion.Logic
{
    public class AGDaemon
    {
        private readonly string whatsappNumber;
        private int ultimaConfiguracionDeEspera;

        public AGDaemon(string whatsappNumber)
        {
            this.whatsappNumber = whatsappNumber;
        }

        public bool DaemonActive { get; set; }

        public void Run(bool silentMode = false)
        {
            try
            {
                var minutesToSleep = DBHelper.GetAutomatedMinutes(whatsappNumber);
                this.ultimaConfiguracionDeEspera = minutesToSleep;

                var userCred = DBHelper.GetCredentialsForUser(whatsappNumber);

                if (!silentMode)
                    WhatsAppService.SendMessage(whatsappNumber, "✅ Se configuró de manera automatica la recuperacion de mensajes\n" +
                                                $"⏳ Tiempo entre actualizaciones configurado: {minutesToSleep} minutos");

                while (DaemonActive)
                {
                    Console.WriteLine($"[DAEMON][{userCred.WhatsappNumber}] Running for: {userCred.WhatsappNumber}");

                    var gestorAG = new AutogestionService();

                    var mensajes = gestorAG.ObtenerMensajes(userCred.Legajo, userCred.Password, userCred.Especialidad);


                    AsyncFunctions.EnviarMensajesAutogestion(whatsappNumber, mensajes, daemonCheck: true);


                    Thread.Sleep(minutesToSleep * 60 * 1000);

                    minutesToSleep = DBHelper.CanAutomatize(whatsappNumber); //devuelve 0 si está desactivado

                    if (minutesToSleep != ultimaConfiguracionDeEspera && minutesToSleep != 0)
                    {
                        ultimaConfiguracionDeEspera = minutesToSleep;
                        WhatsAppService.SendMessage(whatsappNumber, $"🛠 Se ajustó tu frecuencia de actualizacion a: {ultimaConfiguracionDeEspera} minutos!");
                    }
                    if (minutesToSleep <= 0) DaemonActive = false; //saca del ciclo al demonio
                }

                Console.WriteLine($"[DAEMON] Fin del proceso para user: {whatsappNumber}");
            }
            catch (InvalidCredentialException e)
            {
                Console.WriteLine($"[CREDENTIALS] {e.GetType().Name}: {e.Message}\n{e.StackTrace}");
                WhatsAppService.SendMessage(whatsappNumber, $"{e.Message}\nEnviá login con la contraseña correcta nuevamente.");
            }
            catch (AutogestionException e)
            {
                Console.WriteLine($"[CREDENTIALS] {e.GetType().Name}: {e.Message}\n{e.StackTrace}");
                WhatsAppService.SendMessage(whatsappNumber,
                    "‼Ocurrió un problema mientras procesabamos tu solicitud. \n\n❌Puede ser porque autogestión no esté disponible en este momento. \n\n" +
                    "Recordá que si cambiaste la contraseña el bot no tiene forma de averiguarlo. Si es así, logueate de nuevo con _*login*_ !\n" +
                    "Gracias por usar el bot, y lamentamos el error 🤗");
            }
            catch (Exception e)
            {
                Console.WriteLine($"[CREDENTIALS] {e.GetType().Name}: {e.Message}\n{e.StackTrace}");
                WhatsAppService.SendMessage(whatsappNumber, "Ocurrió un error, contacte a los devs: " + e.Message);
            }
        }
    }
}
