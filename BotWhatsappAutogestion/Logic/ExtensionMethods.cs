﻿using BotWhatsappAutogestion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotWhatsappAutogestion.Logic
{
    public static class ExtensionMethods
    {
        public static List<ModelMessage> RemoveOldMessages(this List<ModelMessage> mensajes, long lastDate)
        {

            mensajes.RemoveAll(m => m.FechaPublicacionCompleta <= lastDate);

            mensajes = mensajes.OrderBy(o => o.FechaPublicacionCompleta).ToList();

            return mensajes;
        }

    }
}
