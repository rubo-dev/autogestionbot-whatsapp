﻿using BotWhatsappAutogestion.Models;
using BotWhatsappAutogestion.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Twilio.AspNet.Common;
using System.Net;

namespace BotWhatsappAutogestion.Logic
{
    public static class AsyncFunctions
    {
        public static async void GenerarMensajeLogin(SmsRequest request)
        {
            try
            {
                var mensaje = request.Body;

                if (string.IsNullOrEmpty(mensaje)) return;

                // /login 74533 contra esp
                if (mensaje.Split(" ").Length != 4)
                {
                    WhatsAppService.SendMessage(request.From,
                        "Revisá la sintaxis del comando, enviá _*login*_ o _*ayuda*_ para mas info, o es posible que tu contrañena tenga espacios");
                    return;
                }

                //if (botClient.GetChatAsync(eventArgs.Message.Chat.Id).GetAwaiter().GetResult().PinnedMessage != null) botClient.UnpinChatMessageAsync(eventArgs.Message.Chat.Id);
                var cred = mensaje.Substring(6); //quita el login·

                var user = cred.Split(" ")[0];
                var pwd = cred.Split(" ")[1];
                var esp = cred.Split(" ")[2];

                try
                {
                    DBHelper.NewLogin(request.From, user, pwd, esp);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"[MESSAGE GENERATION] EXCEPTION {e.Message}\n{e.StackTrace}");
                    WhatsAppService.SendMessage(request.From, e.Message);
                    return;
                }

                var messageResult = WhatsAppService.SendMessage(request.From,
                    "El login en el bot fue exitoso✅,\n\nSi querés buscar mensajes nuevos mandá la palabra _*actualizar*_." +
                    "\n\nTambien podes usar el comando _*automatico*_ para que el bot solo por vos busque los mensajes (funcion limitada)\n\nTené en cuenta que el bot " +
                    "aun está en fase de pruebas y con un hosteo de bajo presupuesto.\n" +
                    $"Cualquier sugerencia es bienvenida.\nPodés ver el contacto de los devs mandando _*ayuda*_\n\n\n_botversion: v{BotSettings.Version}_");
            }
            catch (Exception e)
            {
                Console.WriteLine($"[MESSAGE GENERATION] EXCEPTION {e.Message}\n{e.StackTrace}");
                WhatsAppService.SendMessage(request.From,
                    "‼Ocurrió un problema mientras procesabamos tu solicitud. \n\n❌Puede ser porque autogestión no esté disponible en este momento. \n\n" +
                    "Recordá que si cambiaste la contraseña el bot no tiene forma de averiguarlo. Si es así, logueate de nuevo con _*login*_ !\n" +
                    "Gracias por usar el bot, y lamentamos el error 🤗");
            }
        }

        public static async void EnviarMensajesAutogestion(string whatsappNumber, List<ModelMessage> mensajes, bool daemonCheck = false)
        {
            try
            {
                string messageIdInicio = string.Empty;

                if (!daemonCheck)
                {
                    var messageResult = WhatsAppService.SendMessage(whatsappNumber, "🔄 Buscando nuevos mensajes...");
                    messageIdInicio = messageResult.Sid;
                }

                var fechaUltimoMensajeUsuario = DBHelper.ObtenerUltimaFechaMensajes(whatsappNumber);

                mensajes = mensajes.RemoveOldMessages(fechaUltimoMensajeUsuario);

                foreach (var m in mensajes)
                {

                    var output = m.ToString();

                    // TODO: Conseguir lugar para subir documentos porque la API de WhatsApp te pide la URL del recurso
                    // List<Uri> documentos;
                    if (m.TieneAdjunto == 1)
                    {
                        string urlWA = $"https://wa.me/{BotSettings.BotPhoneNumber}?text=descargar%20{WebUtility.UrlEncode(m.Id.ToString())}%20{WebUtility.UrlEncode(m.ArchivoFisico)}";

                        output += $"\n\n*Este mensaje tiene adjunto el archivo {m.ArchivoFisico}*\n\n" +
                            $"Entrá al siguiente link, te va a escribir un mensaje de WhatsApp a este número, envialo tal cual está y recibirás el archivo\n\n{urlWA}";
                    }
                    Thread.Sleep(1100); // hay que esperar mas de un segundo porque tiene un límite de una respuesta por segundo
                    WhatsAppService.SendMessage(whatsappNumber, output);
                }

                if (mensajes.Count == 0)
                {
                    if (!daemonCheck)
                    {
                        WhatsAppService.SendMessage(whatsappNumber, "No se encontraron nuevos mensajes");
                    }
                }
                else
                {
                    WhatsAppService.SendMessage(whatsappNumber, "Si querés buscar mensajes nuevos mandá la palabra _*actualizar*_");
                    DBHelper.ActualizarFechaUltimoMensaje(whatsappNumber, mensajes.Last().FechaPublicacionCompleta);

                }

                Console.WriteLine($"[SYSTEM] End of Request for {whatsappNumber} - Se enviaron: {mensajes.Count} nuevos mensajes");
            }
            catch (Exception e)
            {
                Console.WriteLine($"[SYSTEM] EXCEPTION {e.Message}\n{e.StackTrace}");
                WhatsAppService.SendMessage(whatsappNumber,
                    "‼Ocurrió un problema mientras procesabamos tu solicitud. \n❌Puede ser porque autogestión no esté disponible en este momento. \n\n" +
                    "Recordá que si cambiaste la contraseña el bot no tiene forma de averiguarlo. Si es así, logueate de nuevo con _*login*_ !\n" +
                    "Gracias por usar el bot, y lamentamos el error 🤗");
            }
        }

        public static async void SendBootMessage()
        {
            if (BotSettings.SendBootMessage)
            {
                string mensajeParaEnviar = BotSettings.BootMessage;
                string version = BotSettings.Version;

                List<string> todosLosUsuarios = DBHelper.GetAllWhatsAppNumbers();

                mensajeParaEnviar += $"\n\n_botversion: v{version}_";

                Console.WriteLine($"[SYSTEM] Listo para enviar el mensaje de boot. Procediendo en 1min\nMensaje: {mensajeParaEnviar}");
                Thread.Sleep(30000);
                Console.WriteLine("[SYSTEM] BootMessage: Procediendo en 30s...");
                Thread.Sleep(20000);
                Console.WriteLine("[SYSTEM] BootMessage: Procediendo en 10s...");
                Thread.Sleep(10000);
                Console.WriteLine("[SYSTEM] BootMessage: Procediendo...");



                foreach (var chat in todosLosUsuarios)
                {
                    WhatsAppService.SendMessage(chat, mensajeParaEnviar);
                    Console.WriteLine($"[SYSTEM] Enviado BootMessage a {chat}");

                }

                Console.WriteLine("[SYSTEM] Mensaje de boot enviado a todos los usuarios");

            }
        }

        public static async void LoadAllDaemons()
        {
            Console.WriteLine("[SYSTEM] LOADING DEAMONS...");
            var whatsappNumbers = DBHelper.RetrieveAllAutomaticActives();

            foreach (var number in whatsappNumbers)
            {
                var daemon = new AGDaemon(number) { DaemonActive = true };
                new Task(() => daemon.Run(true)).Start();
                Console.WriteLine($"[DAEMON] Created for: {number}\n[DAEMON] Waiting 30secs...");
                Thread.Sleep(30 * 1000);
            }

            Console.WriteLine("[SYSTEM] ALL DAEMONS LOADED!");
        }

        public static async void EnviarArchivoRequest(SmsRequest request, string baseUrl)
        {
            string senderWhatsappNumber = request.From;
            string mensaje = request.Body;

            if(mensaje.Split(" ").Length != 3)
            {
                WhatsAppService.SendMessage(senderWhatsappNumber, "Recordá enviar el mensaje de descarga tal cual lo genera el link");
                return;
            }

            var cred = mensaje.Substring(10); //quita el descargar·

            var idDocumentoADescargar = cred.Split(" ")[0];
            var fileName = cred.Split(" ")[1];

            // para probar local hay que usar ngrok, hay que cambiar esa URL cada vez que generás un tunel de ngrok nuevo
            //baseUrl = "https://4af3001a3b64.ngrok.io";

            string uriString = $"{baseUrl}/download?id={WebUtility.UrlEncode(idDocumentoADescargar)}&whatsappNumber={WebUtility.UrlEncode(senderWhatsappNumber)}&fileName={WebUtility.UrlEncode(fileName)}";

            var mediaUrl = new[] {new Uri(uriString)}.ToList();

            WhatsAppService.SendMessage(senderWhatsappNumber, fileName, mediaUrl);

            return;
        }
    }
}
