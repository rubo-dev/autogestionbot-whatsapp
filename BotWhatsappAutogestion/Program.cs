using BotWhatsappAutogestion.Logic;
using BotWhatsappAutogestion.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotWhatsappAutogestion
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Out.WriteLine("[SYSTEM] Starting...");
            try
            {
                BotSettings.ConfigureServices();

                Console.WriteLine($"[SYSTEM] BOT: WhatsApp Number {BotSettings.BotPhoneNumber}");

                AsyncFunctions.SendBootMessage();

                if (BotSettings.LoadDaemons)
                {
                    var loadDeamons = new Task(() => AsyncFunctions.LoadAllDaemons());
                    loadDeamons.Start();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"[SYSTEM] EXCEPTION {e.Message}\n{e.StackTrace}");
            }

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
