﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Twilio;

namespace BotWhatsappAutogestion.Services
{
    public static class BotSettings
    {
        public static string AccountSid => Environment.GetEnvironmentVariable("TWILIO_ACCOUNT_SID");

        public static string AuthToken => Environment.GetEnvironmentVariable("TWILIO_AUTH_TOKEN");

        public static string BotPhoneNumber => Environment.GetEnvironmentVariable("BOT_PHONE_NUMBER");

        public static string AesIV256 => Environment.GetEnvironmentVariable("AES_IV_256");

        public static string AesKey256 => Environment.GetEnvironmentVariable("AES_KEY_256");

        public static string DBConnectionString
        {
            get
            {
                var host = Environment.GetEnvironmentVariable("HOST_DB");
                var nameDB = Environment.GetEnvironmentVariable("NAME_DB");
                var user = Environment.GetEnvironmentVariable("USER_DB");
                var pwd = Environment.GetEnvironmentVariable("PWD_DB");

                var output = $"Host={host};Database={nameDB};Username={user};Password={pwd};SSL Mode=Require;TrustServerCertificate=True";

                return output;
            }
        }

        public static string Version => Environment.GetEnvironmentVariable("VERSION");

        public static bool LoadDaemons => Environment.GetEnvironmentVariable("LOAD_DAEMONS").Equals("1");

        public static bool SendBootMessage => Environment.GetEnvironmentVariable("SEND_BOOTMESSAGE").Equals("1");

        public static string BootMessage => Environment.GetEnvironmentVariable("BOOTMESSAGE");

        public static void ConfigureServices()
        {
            TwilioClient.Init(AccountSid, AuthToken);
        }

    }
}
