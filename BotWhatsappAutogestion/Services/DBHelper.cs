﻿using BotWhatsappAutogestion.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BotWhatsappAutogestion.Services
{
    public class DBHelper
    {
        public static void NewLogin(string whatsappNumber, string legajo, string pwd, string esp)
        {
            using (var db = new DBManager())
            {
                var userDB = db.Users.Find(whatsappNumber);
                if (userDB == null)
                {
                    var nuevoLogin = new User
                    {
                        Activo = true,
                        Especialidad = esp,
                        FechaUltimaRequest = DateTime.UtcNow.Subtract(TimeSpan.FromHours(3)),
                        FechaUltimoMensaje = DateTimeOffset.FromUnixTimeMilliseconds(0).DateTime,
                        Legajo = legajo,
                        Password = EncryptionService.Encrypt256(pwd),
                        WhatsappNumber = whatsappNumber
                    };
                    db.Users.Add(nuevoLogin);
                }
                else
                {
                    //ya está en la DB
                    if (!userDB.Activo)
                        throw new Exception("El usuario se encuentra inactivo/baneado para el uso del Bot");
                    userDB.Especialidad = esp;
                    userDB.Legajo = legajo;
                    userDB.Password = EncryptionService.Encrypt256(pwd);
                    userDB.FechaUltimaRequest = DateTime.UtcNow;
                    userDB.FechaUltimoMensaje = DateTimeOffset.FromUnixTimeMilliseconds(0).DateTime;
                }

                db.SaveChanges();
            }
        }

        public static User GetCredentialsForUser(string whatsappNumber)
        {
            using (var db = new DBManager())
            {
                var userDB = db.Users.Find(whatsappNumber);

                if (userDB == null)
                    throw new Exception("El Usuario no se encuentra en la Base de datos, porfavor, realice un _*login*_");

                if (!userDB.Activo)
                    throw new Exception(
                        $"El usuario {userDB.Legajo}@{userDB.Especialidad} se encuentra inhabilitado para el uso del bot");

                var newUserReference = new User
                {
                    Activo = userDB.Activo,
                    Especialidad = userDB.Especialidad,
                    Legajo = userDB.Legajo,
                    Password = EncryptionService.Decrypt256(userDB.Password),
                    AutomaticoActive = userDB.AutomaticoActive,
                    AutomaticoPermitido = userDB.AutomaticoPermitido,
                    TiempoActualizacion = userDB.TiempoActualizacion,
                    WhatsappNumber = userDB.WhatsappNumber,
                    FechaUltimaRequest = userDB.FechaUltimaRequest,
                    FechaUltimoMensaje = userDB.FechaUltimoMensaje
                };


                return newUserReference;
            }
        }

        public static void ActualizarFechaUltimoMensaje(string whatsappNumber, long fechaPublicacionCompleta)
        {
            using (var db = new DBManager())
            {
                var userDB = db.Users.Find(whatsappNumber);
                if (userDB == null) throw new Exception("❗No se encontró el usuario en la DB - Contactar Devs");

                userDB.FechaUltimoMensaje = DateTimeOffset.FromUnixTimeMilliseconds(fechaPublicacionCompleta).DateTime;
                db.SaveChanges();
            }
        }

        public static long ObtenerUltimaFechaMensajes(string whatsappNumber)
        {
            using (var db = new DBManager())
            {
                var userDB = db.Users.Find(whatsappNumber);
                if (userDB == null) throw new Exception("❗No se encontró el usuario en la DB - Contactar Devs");
                var t = (long)userDB.FechaUltimoMensaje.ToUniversalTime()
                    .Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                db.Dispose();
                return t;
            }
        }

        public static bool IsAuthorizedAutomaticRetrieve(string whatsappNumber)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(whatsappNumber);
                if (user == null) return false;

                return user.Activo && user.AutomaticoPermitido;
            }
        }

        public static int GetAutomatedMinutes(string whatsappNumber)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(whatsappNumber);
                if (user == null) return 0;

                return user.TiempoActualizacion.Value;
            }
        }

        public static bool ActiveUser(string whatsappNumber)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(whatsappNumber);
                if (user == null) return false;

                return user.Activo;
            }
        }

        public static bool AutomaticIsActive(string whatsappNumber)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(whatsappNumber);
                if (user == null) return false;

                return user.AutomaticoActive;
            }
        }

        public static int CanAutomatize(string whatsappNumber)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(whatsappNumber);
                if (user == null) return -1;

                if (user.AutomaticoActive && user.AutomaticoPermitido && user.Activo)
                    return user.TiempoActualizacion.Value;
                return -1;
            }
        }

        public static void SetAutomaticState(string whatsappNumber, bool daemonDaemonActive)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(whatsappNumber);
                if (user == null) return;

                user.AutomaticoActive = daemonDaemonActive;
                db.SaveChanges();
            }
        }

        public static List<string> RetrieveAllAutomaticActives()
        {
            var chats = new List<string>();

            using (var db = new DBManager())
            {
                //if (db.Users.Any()) return chats;
                var usuarios = db.Users.Where(u => u.Activo && u.AutomaticoActive && u.AutomaticoPermitido);

                foreach (var user in usuarios) chats.Add(user.WhatsappNumber);
                db.Dispose();
            }

            return chats;
        }

        public static void UpdateLastRequestDate(string whatsappNumber)
        {
            using (var db = new DBManager())
            {
                var user = db.Users.Find(whatsappNumber);
                if (user == null) return;

                user.FechaUltimaRequest = DateTime.UtcNow;
                db.SaveChanges();
            }
        }

        public static DateTime GetLastRequestDate(string whatsappNumber)
        {
            var lastDate = DateTime.MinValue;
            using (var db = new DBManager())
            {
                var user = db.Users.Find(whatsappNumber);
                if (user == null) return DateTime.MinValue;

                lastDate = user.FechaUltimaRequest;

                db.Dispose();
            }

            return lastDate;
        }

        public static List<string> GetAllWhatsAppNumbers()
        {
            List<string> chatsIds = new List<string>();

            using (DBManager db = new DBManager())
            {
                var dbusers = db.Users;
                foreach (var user in dbusers)
                {
                    chatsIds.Add(user.WhatsappNumber);
                }

                return chatsIds;
            }


        }


    }
}
