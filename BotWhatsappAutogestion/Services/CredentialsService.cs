﻿using BotWhatsappAutogestion.Exceptions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace BotWhatsappAutogestion.Services
{
    public class CredentialsService
    {
        public static void ObtenerCredenciales(string usr, string pwd, string esp, out string a4data_param, out string finalcookie)
        {
            var client = new RestClient("https://www.frc.utn.edu.ar/funciones/sesion/iniciarSesion.frc");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            client.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36 Edg/89.0.774.68";
            request.AddHeader("DNT", "1");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            request.AddHeader("Cookie", "Sesion=ID=NULL; SesionID=NULL; ASPSESSIONIDSSSCSBQA=NULL; AUTENTIFICADO=1; MAXIMO=; rec=1; tipoUsuario=A");
            request.AddParameter("page", "login");
            request.AddParameter("pwdClave", pwd);
            request.AddParameter("rec", "on");
            request.AddParameter("redir", "/logon.frc");
            request.AddParameter("t", "79845687");
            request.AddParameter("txtDominios", esp.ToLower());
            request.AddParameter("txtUsuario", usr);
            request.AddParameter("userid", "userid");
            var response = client.Execute(request);
                
            var cookiesDictionary = response.Cookies.ToDictionary(x => x.Name, x => x.Value);

            if (!cookiesDictionary.ContainsKey("Sesion") || !cookiesDictionary.ContainsKey("SesionID")) throw new InvalidCredentialException("Datos de login incorrectos"); // TODO: ver como manejar esto

            Console.WriteLine($"[CREDENTIAL SERVICE] Login correcto {usr}");

            var cookieSesion = cookiesDictionary["Sesion"];
            var cookieSesionID = cookiesDictionary["SesionID"];

            //format Cookie string

            var stringCookie = $"SesionID={cookieSesionID}; Sesion={cookieSesion}";

            //----A4 BASE
            var client3 = new RestClient("https://a4.frc.utn.edu.ar/4");
            client.Timeout = -1;
            var request3 = new RestRequest(Method.GET);
            request3.AddHeader("Cookie", stringCookie);
            var response3 = client3.Execute(request3);


            //parsing JS (A4-data)

            var a4Data = response3.Content.Split("var imagenPerfil =")[0];
            a4Data = a4Data.Split("var A4Data = '")[1];
            a4Data = a4Data.Remove(a4Data.Length - 4);

            a4data_param = a4Data;

            var cookiesA4Dictionary = response3.Cookies.ToDictionary(x => x.Name, x => x.Value);

            if (!cookiesA4Dictionary.ContainsKey("JSESSIONID")) throw new Exception("Autogestión 4 está caída"); // TODO: ver como manejar esto

            Console.WriteLine($"[CREDENTIAL SERVICE] Ingreso A4 correcto {usr}");

            var cookieJSESSIONID = cookiesA4Dictionary["JSESSIONID"];

            finalcookie = $"JSESSIONID=\\{cookieJSESSIONID}\\; {stringCookie}";
        }

    }
}
