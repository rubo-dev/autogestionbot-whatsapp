﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Twilio.Rest.Api.V2010.Account;

namespace BotWhatsappAutogestion.Services
{
    public static class WhatsAppService
    {
        public static MessageResource SendMessage(string whatsappNumber, string text, List<Uri> adjuntos = null)
        {
            MessageResource message;
            if (adjuntos == null)
            {
                message = MessageResource.Create(
                   body: text,
                   from: new Twilio.Types.PhoneNumber($"whatsapp:{BotSettings.BotPhoneNumber}"),
                   to: new Twilio.Types.PhoneNumber(whatsappNumber)
                );
            }
            else
            {
                message = MessageResource.Create(
                   mediaUrl: adjuntos,
                   body: text,
                   from: new Twilio.Types.PhoneNumber($"whatsapp:{BotSettings.BotPhoneNumber}"),
                   to: new Twilio.Types.PhoneNumber(whatsappNumber)
                );
            }

            return message;
        }
    }
}
