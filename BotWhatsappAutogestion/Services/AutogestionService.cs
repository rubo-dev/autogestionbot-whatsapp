﻿using BotWhatsappAutogestion.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace BotWhatsappAutogestion.Services
{
    public class AutogestionService
    {
        public string A4_Data { get; set; }

        public List<ModelMessage> ObtenerMensajes(string user, string password, string especialidad)
        {
            Console.WriteLine($"[AUTOGESTION] Retrieve Started for Legajo:{user}");

            var a4Data = string.Empty;
            var nuevaCookie = string.Empty;

            CredentialsService.ObtenerCredenciales(user, password, especialidad, out a4Data, out nuevaCookie);

            A4_Data = a4Data;
            //----A4 Messages

            var client2 = new RestClient("https://a4.frc.utn.edu.ar/4/mensajes");
            client2.Timeout = -1;
            var request2 = new RestRequest(Method.GET);
            request2.AddHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            request2.AddHeader("A4-Data", a4Data);
            //request2.AddHeader("Cookie", "JSESSIONID=\"B4DLOnbiFX0P+p1BnzH1gWhl.a4server1:node1-1\"; Sesion=ID=%14Y%5F%2DW%2B%2B%5D%2DB%2D%5B%2EVB%5B%2B%2CWB%2D%5E%5F%5DB%5CXXV%5BVXZ%2D%5E%5E%29%12; SesionID=J60B8DD2B%2DB4A9%2D4DC8%2DB102%2D37794975B11FP");
            request2.AddHeader("Cookie", nuevaCookie);

            var response2 = client2.Execute(request2);

            var mensajes = JsonConvert.DeserializeObject<List<ModelMessage>>(response2.Content.Trim()
                .Replace("\n", "").Replace("\t", "").Replace("\r", "").Replace("  ", ""));

            Console.WriteLine($"[AUTOGESTION] Retrieve Finished for Legajo:{user}");

            return mensajes;
        }

        public static Stream DescargarDocumento(string user, string password, string especialidad,
            string idDocumento)
        {
            var a4Data = string.Empty;
            var nuevaCookie = string.Empty;

            CredentialsService.ObtenerCredenciales(user, password, especialidad, out a4Data, out nuevaCookie);


            var client = new RestClient($"https://a4.frc.utn.edu.ar/4/archivo/descargar/{idDocumento}");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            client.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36 Edg/89.0.774.68";
            request.AddHeader("Cookie", nuevaCookie + "; fileDownload=true");
            request.AddHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            request.AddParameter("A4-Data", a4Data);
            var response = client.Execute(request);

            var file = response.RawBytes;

            var stream = new MemoryStream(file);

            return stream;
        }

        public static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }



    }
}
